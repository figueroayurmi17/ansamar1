<?php
	//include_once 'controller/control.php';
?>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="resources/css/bootstrap.css">
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<h3>Planificacion de atencion en consultorios</h3>
			</div>
		</div>
		<div class="">
			<a href="?c=planificacion" class="btn btn-block btn-success">Nueva Planificacion</a>
			<a href="index.php" class="btn btn-block btn-danger">Salir</a>
		</div>
		
		<br>
		<div class="row">
			<div class="col-md-12 text-center">
				<table class="table">
					<tr class="table-secondary">
					
						<th>Consultorio</th>
						<th>turno</th>
						<th>Odontologo</th>
						<th></th>
						<th></th>
						
						
						
					</tr>
					<?php foreach ($this->mode->listarPlanificacion() as $k) : ?>
						
						<tr>
							<td><?php echo $k->consultorio; ?></td>
							<td><?php echo $k->descripcion; ?></td>
							<td><?php echo $k->nombre; ?> <?php echo $k->apellido; ?></td>
						
							<!--<td>
								<a href="?c=nuevaDonacion&id=<?php echo $k->id; ?>" class="btn btn-primary">>Editar<</a>
							</td>
							<td>
								<a href="?c=asignarAmigo&id=<?php echo $k->id;?>" class="btn btn-warning">AsignarAmigo</a>
							</td>
							<td>
								<a href="?c=amigosGrupoFam&id=<?php echo $k->id; ?>" class="btn btn-info">DetallesGrupo</a>
							</td>
							<td>
								<a href="?c=asignarDonacion&id=<?php echo $k->id; ?>" class="btn btn-info">Asignar</a>
							</td>
							<td>
								<a href="?c=inhabilitarDonacion&id=<?php echo $k->id; ?>" class="btn btn-primary">>Editar<</a>
							</td>-->
							<td>
								<a href="?c=eliminarPlanificacion&id=<?php echo $k->id; ?>" class="btn btn-danger">Eliminar</a>
							</td>

						</tr>

				<?php endforeach; ?>
					
				</table>
				<!--<div class="row">
				<a href="?c=nuevaDonacion" class="btn btn-block btn-success">Nuevo Registro</a>
				</div>-->
				
			</div>
		</div>
	</div>

</body>
</html>