<!DOCTYPE html>
<html>
<head>
	<!--<link rel="stylesheet" type="text/css" href="resources/css/bootstrap.css">
	<script type="text/javascript" src="resources/js/bootstrap.min.js"></script>-->
	<link rel="stylesheet" href="resources/css/bootstrap.css">
	<script type="text/javascript" src="resources/js/bootstrap.min.js"> </script>
</head>
<body>
    <br>
<div class="row justify-content-center">
<div class="card text-dark bg-light mb-3" style="max-width: 40rem;">
  <div class="card-header">Registrar Donacion</div>
  <div class="card-body">
					<form class="form-horizontal" method="post" action="?c=guardarDonacion">
                        
                        <div class="col-md-8">
                            <input type="hidden" name="txtIdDonacion" value="<?php echo $alm->id; ?>">
                            <label>Detalles de donación: </label>
                            <br>
                            <!--<input type="text" class="form-control" name="nombreGrupo" id="nombreGrupo" aria-describedby="emailHelp" placeholder="Donacion de ropa para niñ@s" value="<?php echo $alm->nombre; ?>">-->
                            <textarea name="detallesDonacion" id="detallesDonacion"><?php echo $alm->detalles; ?></textarea>
                            
                        </div>
                        <div class="col-md-8">
                            <label>Cantidad: </label>
                            <br>
                            <input type="text" class="form-control" name="cantidad" id="cantidad"  aria-describedby="emailHelp" placeholder="Ej. 50 piezas" value="<?php echo $alm->cantidad; ?>">
                        </div>
                        <div class="col-md-8">
                            <label>Donante: </label>
                            <select name="selectDonante">
                                <?php foreach ($this->mode->listarTodos()  as $k) : ?>
                                    <option value="<?php echo $k->id ?>" <?php echo $k->id == $alm->donante_id ? 'selected' : ''; ?>><?php echo $k->nombre." ".$k->apellido ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-md-8">
                            <label>Fecha de Donacion: </label>
                            <input type="date" class="form-control" name="fechaDonacion" id="fechaDonacion" value="<?php echo $alm->fecha; ?>" aria-describedby="emailHelp">
                        </div>
                  
                        
                        <br>
                        <div>
                        <!--<a href="?c=guardar" class="btn btn-block btn-success">Guardar</a>-->
						<button type="submit" value="Guardar" class="btn btn-success">Registrar</button>
                        <a href="index.php?c=listarDonaciones" class="btn btn-block btn-danger">Cancelar</a>
                        </div>
                        <div>
                        
                        </div>
			</form>
  </div>
</div>
</div>









	
	<!--<script type="text/javascript" src="resources/js/Jquery.js"></script>
	<script type="text/javascript" src="resources/js/materialize.js"></script>-->
	<script type="text/javascript">
		$(document).ready(function(){
			$('select').formSelect();
		});
	</script>
</body>
</html>