<!DOCTYPE html>
<html>
<head>
    <!--<link rel="stylesheet" type="text/css" href="resources/css/bootstrap.css">
    <script type="text/javascript" src="resources/js/bootstrap.min.js"></script>-->
    <link rel="stylesheet" href="resources/css/bootstrap.css">
    <script type="text/javascript" src="resources/js/bootstrap.min.js"> </script>
    <link rel="stylesheet" type="text/css" href="resources/alertifyjs/css/alertify.css">
    <link rel="stylesheet" type="text/css" href="resources/alertifyjs/css/themes/default.css">
</head>
<body>
    <br>
<div class="row justify-content-center">
<div class="card text-dark bg-light mb-3" style="max-width: 40rem;">
  <div class="card-header">Editar Cliente</div>
  <div class="card-body">
                    <form class="form-horizontal" method="post" action="?c=guardar1">
                        <div class="col-md-8">
                        <input type="hidden" name="txtID" id="txtID" value="<?php echo $alm->id; ?>">
                        <input type="hidden" name="cedulaAmigo" id="cedulaAmigo" value="<?php echo $alm->cedula; ?>">
                            <input type="text" class="form-control" name="cedula" id="cedula" onkeyup= keepNumOrDecimal(this) value="<?php echo $alm->cedula; ?>" aria-describedby="emailHelp" placeholder="22186490" maxlength="8" required>
                        </div>
              
                        <div class="col-md-8">
                            <input type="text" style="text-transform:uppercase;" onkeypress="return validar(event)" name="nombres" class="form-control mayusculas buscar" id="nombres" value="<?php echo $alm->nombresApellidos; ?>" aria-describedby="emailHelp" placeholder="Nombres" maxlength="25" required>
                        </div>
                        <!--<div class="col-md-8">
                            <input type="text" style="text-transform:uppercase;" onkeypress="return validar(event)" class="form-control mayusculas buscar" name="apellidos" id="apellidos" value="<?php echo $alm->nombresApellidos; ?>" aria-describedby="emailHelp" placeholder="Apellidos" maxlength="25" required>
                        </div>-->
                       
                        <div class="col-md-8">
                            <input type="text" style="text-transform:uppercase;" class="form-control" name="direccion" id="direccion" value="<?php echo $alm->direccion; ?>" aria-describedby="emailHelp" placeholder="Direccion" maxlength="80" required>
                        </div>

                        <div class="col-md-8">
                            <input type="text" style="text-transform:uppercase;" class="form-control" name="correo" id="correo" value="<?php echo $alm->correo; ?>" aria-describedby="emailHelp" placeholder="example@gmail.com" maxlength="80" required>
                        </div>

                        <div class="col-md-8">
                            <!--<select name="codTlfn" id="codTlfn" required>
                                <option value="<?php echo $alm->codTlfn; ?>"><?php echo $alm->codTlfn; ?></option>
                                <option value="0412">0412</option>
                                <option value="0414">0414</option>
                                <option value="0424">0424</option>
                                <option value="0416">0416</option>
                                <option value="0426">0426</option>
                                
                            </select>-->
                            <input type="text" class="form-control" name="telefono" onkeyup= keepNumOrDecimal(this) id="telefono" value="<?php echo $alm->tlfn; ?>" aria-describedby="emailHelp" placeholder="04245208619" maxlength="11" required>
                        </div>

                        <div class="col-md-8">
                            <label name="status" id="status" >Status</label>
                            <select name="status" id="status" required>
                            <option value="<?php echo $alm->status; ?>"></option>
                                <option value="1">Activo</option>
                                <option value="0">Inactivo</option>
                            </select>
                        </div>
                        


                        <br>
                        <div>
                        <!--<a href="?c=guardar" class="btn btn-block btn-success">Guardar</a>-->
                        <button type="submit" value="Guardar" id="registrar" class="btn btn-primary">Actualizar</button>
                        
                        <a href="index.php" class="btn btn-block btn-danger">Cancelar</a>
                        </div>
                        <div>
                        
                        </div>
            </form>
  </div>
</div>
</div>









    
    <!--<script type="text/javascript" src="resources/js/Jquery.js"></script>
    <script type="text/javascript" src="resources/js/materialize.js"></script>-->

    <!--<script type="text/javascript">
        $(document).ready(function(){
            $('select').formSelect();
            var id = document.getElementById("txtID").value;
            console.log(id);
        });
    </script>-->

    <script type="text/javascript" src="resources/js/Jquery.js"></script>
    <script language="JavaScript" type="text/javascript">
            $(document).ready(function(){
                var id = document.getElementById("txtID").value;                /*$('#registrar').click(function(){
                    document.getElementById('cedula').value="";
                });*/
                var prueba = document.getElementById("cedula").value;
                if(prueba==""){
                    console.log("cedula vacia");
                }else{
                    console.log("cedula llena");
                }
                $('#migrarAmigo').click(function(){
                    var x = document.getElementById('migrarAmigo').value;
                    console.log(x);
                    if(x == 2){
                        document.getElementById("miembro").disabled=false;
                        $('#fechaMig').show();
                        $('#fechaMigLabel').show();
                        $('#fechaBau').show();
                        $('#fechaBauLabel').show();
                        $('#membresia').show();
                        $('#selectCargo').show();
                        $('#estatus').show();
                        $('#membresiaLabel').show();
                        $('#cargoLabel').show();
                        $('#vehiculoLabel').show();
                        $('#selectVehiculo').show();
                        $('#nivelEducativoLabel').show();
                        $('#selectNivelEducativo').show();
                        $('#profesionLabel').show();
                        $('#profesion').show();
                        
                        document.getElementById("registrar").disabled=true;
                    }
                    if(x == 1){
                        document.getElementById("miembro").disabled=true;
                        $('#fechaMig').hide();
                        $('#fechaMigLabel').hide();
                        $('#fechaBau').hide();
                        $('#fechaBauLabel').hide();
                        $('#membresia').hide();
                        $('#selectCargo').hide();
                        $('#estatus').hide();
                        $('#membresiaLabel').hide();
                        $('#cargoLabel').hide();
                        $('#vehiculoLabel').hide();
                        $('#selectVehiculo').hide();
                        $('#nivelEducativoLabel').hide();
                        $('#selectNivelEducativo').hide();
                        $('#profesionLabel').hide();
                        $('#profesion').hide();
                        document.getElementById("registrar").disabled=false;
                    }
                });
                if(id > 0){
                    document.getElementById("miembro").disabled=true;
                    $('#fechaMig').hide();
                    $('#fechaMigLabel').hide();
                    $('#fechaBau').hide();
                    $('#fechaBauLabel').hide();
                    $('#membresia').hide();
                    $('#selectCargo').hide();
                    $('#estatus').hide();
                    $('#membresiaLabel').hide();
                    $('#cargoLabel').hide();
                    $('#vehiculoLabel').hide();
                    $('#selectVehiculo').hide();
                    $('#nivelEducativoLabel').hide();
                    $('#selectNivelEducativo').hide();
                    $('#profesionLabel').hide();
                    $('#profesion').hide();
                    console.log("Puede pasar");
                }
                else {
                    document.getElementById("miembro").disabled=true;
                    $('#fechaMig').hide();
                    $('#fechaMigLabel').hide();
                    $('#migrarAmigo').hide();
                    $('#migrarAmigoLabel').hide();
                    $('#fechaBau').hide();
                    $('#fechaBauLabel').hide();
                    $('#membresia').hide();
                    $('#selectCargo').hide();
                    $('#estatus').hide();
                    $('#membresiaLabel').hide();
                    $('#cargoLabel').hide();
                    $('#vehiculoLabel').hide();
                    $('#selectVehiculo').hide();
                    $('#nivelEducativoLabel').hide();
                    $('#selectNivelEducativo').hide();
                    $('#profesionLabel').hide();
                    $('#profesion').hide();
                }
            });
              
</script>

<script>
    
    // Forzar solo números y puntos decimales
    function keepNumOrDecimal(obj) {
	 // Reemplace todos los no numéricos primero, excepto la suma numérica.
	obj.value = obj.value.replace(/[^\d.]/g,"");
	 // Debe asegurarse de que el primero sea un número y no.
	obj.value = obj.value.replace(/^\./g,"");
	 // Garantizar que solo hay uno. No más.
	obj.value = obj.value.replace(/\.{2,}/g,".");
	 // Garantía. Solo aparece una vez, no más de dos veces
	obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
    }
    </script>

	<script type="text/javascript"> // VALIDAR CAMPOS DE SOLO NUMERO Y LETRAS AL INPUT
                          //jQuery('.soloNumeros').keypress(function (tecla) {
                          //if (tecla.charCode < 48 || tecla.charCode > 57) return false;
                          //});
                          
                          $("input.buscar").bind('keypress', function(event) {
                          var regex = new RegExp("^[a-zA-Z ]+$");
                          var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                          if (!regex.test(key)) {
                          event.preventDefault();
                          return false;
                          }
                          });
    </script>
	
	
    <script type="text/javascript">
        //var id = document.getElementByName("txtID").value;
        //console.log(id);
    </script>
    
</body>
</html>