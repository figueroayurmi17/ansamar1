<!DOCTYPE html>
<html>
<head>
	<!--<link rel="stylesheet" type="text/css" href="resources/css/bootstrap.css">
	<script type="text/javascript" src="resources/js/bootstrap.min.js"></script>-->
	<link rel="stylesheet" href="resources/css/bootstrap.css">
	<script type="text/javascript" src="resources/js/bootstrap.min.js"> </script>
</head>
<body>
    <br>
<div class="row justify-content-center">
<div class="card text-dark bg-light mb-3" style="max-width: 40rem;">
  <div class="card-header">Registrar Actividad</div>
  <div class="card-body">
					<form class="form-horizontal" method="post" action="?c=guardarActividad">
                        
                        <div class="col-md-8">
                            <input type="hidden" name="txtIdActividad" value="<?php echo $alm->id; ?>">
                            <input type="text" class="form-control" name="nombreActividad" id="nombreActividad" aria-describedby="emailHelp" placeholder="Nombre de la Actividad" value="<?php echo $alm->nombre; ?>">
                        </div>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="direccionActividad" id="direccionActividad"  aria-describedby="emailHelp" placeholder="Direccion" value="<?php echo $alm->direccion; ?>">
                        </div>
                        <div class="col-md-8">
                            <input type="date" class="form-control" name="fechaActividad" id="fechaActividad"  aria-describedby="emailHelp" placeholder="fechaActividad" value="<?php echo $alm->fecha; ?>">
                        </div>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="horaActividad" id="horaActividad"  aria-describedby="emailHelp" placeholder="Hora" value="<?php echo $alm->hora; ?>">
                        </div>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="descripcionActividad" id="descripcionActividad"  aria-describedby="emailHelp" placeholder="Descripcion" value="<?php echo $alm->descripcion; ?>">
                        </div>
                        <div class="col-md-8">
                            <label>Lider: </label>
                            <select name="selectLider">
                                <?php foreach ($this->mode->cargarLider()  as $k) : ?>
                                    <option value="<?php echo $k->id_miembro ?>"<?php echo $k->id_miembro == $alm->id_encargado ? 'selected' : '';?>><?php echo $k->nombreLider ?></option>
                                <?php endforeach ?>
                            </select>
                            <!--<select name="selectLider">
                                <?php foreach ($this->mode->cargarLider()  as $k) : ?>
                                    <option value="<?php echo $k->id_miembro ?>" <?php echo $k->id_miembro == $alm->id_encargado ? 'selected' : ''; ?>><?php echo $k->nombreLider ?></option>
                                <?php endforeach ?>
                            </select>-->

                        </div>
                        <!--<div class="col-md-8">
                            <label>Zona: </label>
                            <select name="selectZona">
                                <?php foreach ($this->mode->cargarZonas()  as $k) : ?>
                                    <option value="<?php echo $k->id ?>" <?php echo $k->id == $alm->id_zona ? 'selected' : ''; ?>><?php echo $k->nombre ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>-->


           
                        
                        
                        <br>
                        <div>
                        <!--<a href="?c=guardar" class="btn btn-block btn-success">Guardar</a>-->
						<button type="submit" value="Guardar" class="btn btn-success">Registrar</button>
                        <a href="index.php?c=listarActividades" class="btn btn-block btn-danger">Cancelar</a>
                        </div>
                        <div>
                        
                        </div>
			</form>
  </div>
</div>
</div>









	
	<!--<script type="text/javascript" src="resources/js/Jquery.js"></script>
	<script type="text/javascript" src="resources/js/materialize.js"></script>-->
	<script type="text/javascript">
		$(document).ready(function(){
			$('select').formSelect();
		});
	</script>
</body>
</html>