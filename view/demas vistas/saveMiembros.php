<!DOCTYPE html>
<html>
<head>
	<!--<link rel="stylesheet" type="text/css" href="resources/css/bootstrap.css">
	<script type="text/javascript" src="resources/js/bootstrap.min.js"></script>-->
	<link rel="stylesheet" href="resources/css/bootstrap.css">
	<script type="text/javascript" src="resources/js/bootstrap.min.js"> </script>
	<link rel="stylesheet" type="text/css" href="resources/alertifyjs/css/alertify.css">
	<link rel="stylesheet" type="text/css" href="resources/alertifyjs/css/themes/default.css">
</head>
<body>

    <br>
<div class="row justify-content-center">
<div class="card text-dark bg-light mb-3" style="max-width: 40rem;">
  <div class="card-header">Actualizar Data Miembro</div>
  <div class="card-body">
					<form class="form-horizontal" method="post" action="?c=actualizarMiembro">
                        <div class="col-md-8">
						<input type="hidden" name="txtID" id="txtID" value="<?php echo $alm->id_miembro; ?>">
                        <input type="hidden" name="txtIdAmigo" id="txtIdAmigo" value="<?php echo $alm->amigo_id; ?>">
						<input type="hidden" name="cedulaAmigo" id="cedulaAmigo" value="<?php echo $alm->cedula; ?>">
                            
                            <input type="text" class="form-control" name="cedula" id="cedula" onkeyup= keepNumOrDecimal(this) value="<?php echo $alm->cedula; ?>" aria-describedby="emailHelp" placeholder="22186490" maxlength="8" required>
                        </div>
              
                        <div class="col-md-8">
                            <input type="text" style="text-transform:uppercase;" onkeypress="return validar(event)" name="nombres" class="form-control mayusculas buscar" id="nombres" value="<?php echo $alm->nombre; ?>" aria-describedby="emailHelp" placeholder="Nombres" maxlength="25" required>
                        </div>
                        <div class="col-md-8">
                            <input type="text" style="text-transform:uppercase;" onkeypress="return validar(event)" class="form-control mayusculas buscar" name="apellidos" id="apellidos" value="<?php echo $alm->apellido; ?>" aria-describedby="emailHelp" placeholder="Apellidos" maxlength="25" required>
                        </div>
                        <div class="col-md-8">
                            <label>Genero: </label>
                            <select name="selectGenero" required>
                                <option value="<?php echo $alm->sexo; ?>"><?php echo $alm->sexo; ?></option>
                                <option value="Masculino">Masculino</option>
                                <option value="Femenino">Femenino</option>
                            </select>
                        </div>
                        <div class="col-md-8">
                            <input type="text" style="text-transform:uppercase;" class="form-control" name="direccion" id="direccion" value="<?php echo $alm->direccion; ?>" aria-describedby="emailHelp" placeholder="Direccion" maxlength="80" required>
                        </div>
                        <div class="col-md-8">
                            <select name="codTlfn" id="codTlfn" required>
                                <option value="<?php echo $alm->codTlfn; ?>"><?php echo $alm->codTlfn; ?></option>
                                <option value="0412">0412</option>
                                <option value="0414">0414</option>
                                <option value="0424">0424</option>
                                <option value="0416">0416</option>
                                <option value="0426">0426</option>
                                
                            </select>
                            <input type="text" class="form-control" name="telefono" onkeyup= keepNumOrDecimal(this) id="telefono" value="<?php echo $alm->telefono; ?>" aria-describedby="emailHelp" placeholder="Telefono" maxlength="7" required>
                        </div>

                        <div class="col-md-8">
                            <label for="exampleInputEmail1" class="form-label">Fecha de Nacimiento</label>
                            <input type="date" class="form-control" name="fechaNac" id="fechaNac" value="<?php echo $alm->fecha_nacimiento; ?>" aria-describedby="emailHelp" required>
                        </div>

                        <div class="col-md-8">
                            <label for="exampleInputEmail1" class="form-label">Fecha de Ingreso</label>
                            <input type="date" class="form-control" name="fechaIng" id="fechaIng" value="<?php echo $alm->fecha_ingreso; ?>" aria-describedby="emailHelp" required>
                        </div>
                        <div class="col-md-8">
                            <label for="exampleInputEmail1" class="form-label">Por donde nos conocio?</label>
                            <select name="selectLlegoPor" required>
                                <option value="<?php echo $alm->llegoPor; ?>"><?php echo $alm->llegoPor; ?></option>
                                <option value="Cuenta Propia">Cuenta Propia</option>
                                <option value="Radio">Radio</option>
                                <option value="TV">TV</option>
                                <option value="Redes Sociales">Redes Sociales</option>
                                <option value="Amigo">Amigo</option>
                                <option value="Familiar">Familiar</option>
                            </select>
                        </div>
                       
                        
                        <div class="col-md-8">
                            <label for="exampleInputEmail1" id="fechaMigLabel" class="form-label">Fecha Migracion a Miembro</label>
                            <input type="date" class="form-control" name="fechaMig" id="fechaMig" value="<?php echo $alm->fecha_paso_de_fe; ?>" aria-describedby="emailHelp" required>
                        </div>
                        <div class="col-md-8">
                            <label for="exampleInputEmail1" id="fechaBauLabel" class="form-label">Fecha Bautismo</label>
                            <input type="date" class="form-control" name="fechaBau" id="fechaBau" value="<?php echo $alm->fecha_bautismo; ?>" aria-describedby="emailHelp" required>
                        </div>
                        <div class="col-md-8">
                            <label for="exampleInputEmail1" id="membresiaLabel" class="form-label">Membresia: </label>
                            <select name="membresia" id="membresia" required>
                            <?php foreach ($this->mode->cargarMembresias()  as $k) : ?>
                                <option value="<?php echo $k->id ?>" <?php echo $k->id == $alm->membresia_id ? 'selected' : ''; ?> ><?php echo $k->nombre ?></option>
                            <?php endforeach ?>
                        </select> 
                        </div>
                        <div class="col-md-8">
                            <label for="exampleInputEmail1" id="membresiaLabel" class="form-label">Cargo: </label>
                        	<select name="selectCargo" id="selectCargo" required>
							<?php foreach ($this->mode->cargarCargos()  as $k) : ?>
								<option value="<?php echo $k->id ?>" <?php echo $k->id == $alm->cargo_id ? 'selected' : ''; ?>><?php echo $k->nombre ?></option>
							<?php endforeach ?>
						</select>
                        </div>
                        <div class="col-md-8">
                            <label id="vehiculoLabel">Posee vehiculo?</label>
                        </div>
                        <div class="col-md-8">
                            <select name="selectVehiculo" id="selectVehiculo" required>
                                <option value="<?php echo $alm->vehiculo; ?>"><?php echo $alm->vehiculo; ?></option>
                                <option value="si">si</option>
                                <option value="no">no</option>
                            </select>
                        </div>
                        <div class="col-md-8">
                            <label id="nivelEducativoLabel">Grado de instruccion: </label>
                           
                        </div>
                        <div class="col-md-8">
                            <select name="selectNivelEducativo" id="selectNivelEducativo" required>
                                <option value="<?php echo $alm->nivelAcademico; ?>"><?php echo $alm->nivelAcademico; ?></option>
                                <option value="BACHILLER">BACHILLER</option>
                                <option value="TSUT">TSU</option>
                                <option value="LICENCIADO(A)">LICENCIADO(A)</option>
                                <option value="INGENIERO(A)">INGENIERO(A)</option>
                            </select>
                        </div>
                        <div class="col-md-8">
                            <label id="profesionLabel">Profesion: </label>
                            <input type="text" style="text-transform:uppercase;" onkeypress="return validar(event)" class="form-control mayusculas buscar" name="profesion" id="profesion" aria-describedby="emailHelp" placeholder="mecanico" maxlength="50" value="<?php echo $alm->profesion; ?>" required>
                        </div>
                        


                        <br>
                        <div>
                        <!--<a href="?c=guardar" class="btn btn-block btn-success">Guardar</a>-->
						<button type="submit" value="Guardar" id="registrar" class="btn btn-warning">Actualizar</button>
						
                        <a href="index.php?c=listarMiembros" class="btn btn-block btn-danger">Cancelar</a>
                        </div>
                        <div>
                        
                        </div>
			</form>
  </div>
</div>
</div>
<script type="text/javascript" src="resources/js/Jquery.js"></script>
<script>
    
    // Forzar solo números y puntos decimales
    function keepNumOrDecimal(obj) {
     // Reemplace todos los no numéricos primero, excepto la suma numérica.
    obj.value = obj.value.replace(/[^\d.]/g,"");
     // Debe asegurarse de que el primero sea un número y no.
    obj.value = obj.value.replace(/^\./g,"");
     // Garantizar que solo hay uno. No más.
    obj.value = obj.value.replace(/\.{2,}/g,".");
     // Garantía. Solo aparece una vez, no más de dos veces
    obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
    }
    </script>

    <script type="text/javascript"> // VALIDAR CAMPOS DE SOLO NUMERO Y LETRAS AL INPUT
                          //jQuery('.soloNumeros').keypress(function (tecla) {
                          //if (tecla.charCode < 48 || tecla.charCode > 57) return false;
                          //});
                          
                          $("input.buscar").bind('keypress', function(event) {
                          var regex = new RegExp("^[a-zA-Z]+$");
                          var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                          if (!regex.test(key)) {
                          event.preventDefault();
                          return false;
                          }
                          });
    </script>

</body>
</html>