<?php
	//include_once 'controller/control.php';
?>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="resources/css/bootstrap.css">

	<script src="push.js/bin/push.min.js"></script>

	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<br>
				<br>
				<h3>Centro Odontologico Ansamar</h3>
			</div>
		</div>
		<br>
		
		<div class="">
			<a href="?c=nuevo" class="btn btn-block btn-success">Registrar Cliente</a>
			<a href="?c=asignarDeuda" class="btn btn-block btn-danger">Asignar Deuda</a>
			<a href="?c=asignarPago" class="btn btn-block btn-warning">Asignar Pago</a>
		</div>
		<br>
		<div class="">
			<a href="?c=listarClientesDeudores" class="btn btn-block btn-danger">Listar Clientes Deudores</a>
			<a href="?c=listarClientesDeudoresMas100" class="btn btn-block btn-primary">Listar clientes con deuda mayor a 100$</a>
		</div>
		<br>
		<div class="">
			<!--<a href="?c=nuevoGrupoFam" class="btn btn-block btn-info">Buscar Cliente</a>-->
			<a href="?c=listarClientes" class="btn btn-block btn-primary">Listar clientes</a>
			<a href="?c=listarClientesConMas15DiasSinPagar" class="btn btn-block btn-warning">Listar clientes ULTIMO PAGO</a>
		</div>
		<br>
		<div class="">
			<a href="?c=listarUltimosPagos" class="btn btn-block btn-success">Listar Ultimos Pagos</a>
			<a href="?c=listarUltimasDeudas" class="btn btn-block btn-danger">Listar Ultimas Deudas</a>
		</div>
		<br>
				
				
	</div>

	

</body>

</html>